/*
 *  DataHandler handles all write operations on the local database.
 */

import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import "ModelUtils.js" as ModelUtils


QtObject {
    id: root;
    property bool initialized: false
    property int reqAvailable
    property int reqTimer
    property date apiReopening: new Date()

    Component.onCompleted: {
        var db = init();
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM FetchingLeagueTeams');
        });
    }

    function init() {
        var db = LocalStorage.openDatabaseSync("uFootball_db", "", "Database of football data", 1000000);

        // check tables only on startup
        if (!initialized) {
            db.transaction(function(tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS League(id INTEGER PRIMARY KEY, name TEXT, year INTEGER, currentMatchday INTEGER, numberOfMatchdays INTEGER, lastupdate TIMESTAMP, teamCount INTEGER, lastUpdatedStandings TIMESTAMP, lastFetchedTeams TIMESTAMP, lastFetchedFixtures TIMESTAMP)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS Team(id INTEGER PRIMARY KEY, name TEXT, code TEXT, shortName TEXT, icon TEXT, iconState INTEGER, lastFetched TIMESTAMP, lastFetchedFixtures TIMESTAMP, lastFetchedSquad TIMESTAMP)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS TeamLeague(teamId INTEGER, leagueId INTEGER, FOREIGN KEY(teamId) REFERENCES Team(id), FOREIGN KEY(leagueId) REFERENCES League(id))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS Player(id INTEGER, name TEXT, position TEXT, dateOfBirth TIMESTAMP, countryOfBirth TEXT, nationality TEXT, role TEXT, teamId INTEGER, FOREIGN KEY(teamId) REFERENCES Team(id))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS Standing(leagueId INTEGER, stage TEXT, type TEXT, pool TEXT, rank INTEGER, teamId INTEGER, playedGames INTEGER, points INTEGER, goals INTEGER, goalsAgainst INTEGER, FOREIGN KEY(leagueId) REFERENCES League(id))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS Fixture(id INTEGER PRIMARY KEY, leagueId INTEGER, date TIMESTAMP, matchday INTEGER, homeTeamId INTEGER, awayTeamId INTEGER, homeGoals INTEGER, awayGoals INTEGER, FOREIGN KEY(leagueId) REFERENCES League(id), FOREIGN KEY(homeTeamId) REFERENCES Team(id), FOREIGN KEY(awayTeamId) REFERENCES Team(id))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS Favorite(teamId INTEGER, position INTEGER, FOREIGN KEY(teamId) REFERENCES Team(id))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS Api(key TEXT)');

                // some metadata to reduce API calls
                tx.executeSql('CREATE TABLE IF NOT EXISTS LocalMatchday(leagueId INTEGER, matchday INTEGER)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS FetchingLeagueTeams(leagueId INTEGER)');
            });

            initialized = true;
        }

        return db;
    }

    function requestHelper(request, callback) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "http://api.football-data.org/v2/" + request, true);
        xmlhttp.setRequestHeader("X-Auth-Token", getApiKey());

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                reqAvailable = xmlhttp.getResponseHeader('X-Requests-Available-Minute');
                reqTimer = xmlhttp.getResponseHeader('X-RequestCounter-Reset');
                apiReopening = new Date(new Date().getTime() + reqTimer*1000);
                console.log("[LOG]: REQUEST ", request)
                console.log("[LOG]:    RESPONSE HEADERS")
                console.log("[LOG]:      - Requests left: ", reqAvailable)
                console.log("[LOG]:      - Timer reset in ",reqTimer," s")
                console.log("[LOG]:    RESPONSE STATUS")
                console.log("[LOG]:      -", xmlhttp.status)

                if (xmlhttp.status == 200) {
                    callback(JSON.parse(xmlhttp.responseText));
                }
            }
        }
        if ((reqAvailable === 0 || reqAvailable === 1 )&& new Date() < apiReopening ) {
            console.log("[LOG]: REQUEST DELAYED for",reqTimer ,' seconds - ', request);
            var delay = Qt.createQmlObject("import QtQuick 2.4; Timer {interval: "+reqTimer*1000+"; repeat: false; running: true;}",root,"MyTimer");
            delay.triggered.connect(function(){
                xmlhttp.send()
                console.log("[LOG]: REQUEST SENT", request);
            });
        } else{
            xmlhttp.send();
            console.log("[LOG]: REQUEST SENT", request);
        }
    }

    // called on app startup, this should update the most crucial data
    function startup() {
        var pastweek = ModelUtils.timeTravel('-1')
        var nextweek = ModelUtils.timeTravel('1')
        updateFootballSeasons()
        updateFixturesByTimeFrame(pastweek, nextweek)
    }

    function timer() {
        return Qt.createQmlObject("import QtQuick 2.0; Timer {}",root);
    }

    signal competitionsUpdated()

    // update Seasons, remove old ones, add new ones, update team information
    function updateFootballSeasons() {
        requestHelper("competitions/", function (competitions) {
            var db = init();
            var leagueIds = [];

            db.transaction(function(tx) {

                //Find if the database is empty
                var firstrun = false;
                if (getAllLeagues().length === 0){
                    console.log("[Log] No league in the db");
                    var firstrun = true;
                }

                for (var i = 0; i < competitions.competitions.length; i++) {
                    if (competitions.competitions[i].plan === "TIER_ONE" && competitions.competitions[i].currentSeason.endDate > ModelUtils.timeTravel('-30')) {

                        // to have currentMatchday only in numbers
                        if (competitions.competitions[i].currentSeason.currentMatchday === "") {
                            competitions.competitions[i].currentSeason.currentMatchday === 0
                        }

                        // to define the number of matchdays
                        competitions.competitions[i].numberOfMatchdays = 50

                        leagueIds.push(competitions.competitions[i].id);
                        var rs = tx.executeSql("SELECT id FROM League WHERE id = ?", competitions.competitions[i].id);
                        if (rs.rows.length === 0) {
                            tx.executeSql("INSERT INTO League(id, name, year, lastupdate, teamCount, currentMatchday, numberOfMatchdays, lastFetchedTeams) VALUES(?, ?, ?, ?, ?, ?, ?, date('now', '-10 days'))", [competitions.competitions[i].id, competitions.competitions[i].name, competitions.competitions[i].year, competitions.competitions[i].lastUpdated, competitions.competitions[i].numberOfTeams, competitions.competitions[i].currentSeason.currentMatchday, competitions.competitions[i].numberOfMatchdays]);
                        } else {
                            if (tx.executeSql("SELECT lastupdate FROM League WHERE id = ?", competitions.competitions[i].id).rows.item(0).lastupdate !== competitions.competitions[i].lastUpdated) {
                                console.log("UPDATE League", competitions.competitions[i].id);
                                tx.executeSql("UPDATE League SET year = ?, lastupdate = ?, currentMatchday = ? WHERE id = ?", [competitions.competitions[i].year, competitions.competitions[i].lastUpdated, competitions.competitions[i].currentSeason.currentMatchday, competitions.competitions[i].id]);
                            }
                        }

                        // check for team updates
                        if (firstrun && leagueIds.length === 6){
                            var chrono = new timer();
                            chrono.interval = 100000;
                            chrono.repeat = false;
                            chrono.triggered.connect( (function () {
                                var j = i; // use inline closure (www.pluralsight.com/guides/javascript-callbacks-variable-scope-problem) to keep to good value of i
                                return function () {
                                    updateTeamsByLeague(competitions.competitions[j].id);
                                }
                            })() );
                            chrono.start();
                        } else {
                            updateTeamsByLeague(competitions.competitions[i].id);
                        }
                    }
                }

                var leagues = tx.executeSql("SELECT id FROM League").rows;
                for (var j = 0; j < leagues.length; j++) {
                    if (leagueIds.indexOf(leagues.item(j).id) < 0) {
                        removeFootballSeason(leagues.item(j).id);
                    }
                }
            });
            competitionsUpdated();
        });

    }

    function removeFootballSeason(leagueId) {
        console.log("[LOG] Removing league:", leagueId)

        init().transaction(function(tx) {
            // remove league
            tx.executeSql('DELETE FROM League WHERE id = ?', leagueId);

            // remove all teams, that only show up in this league
            var teams = tx.executeSql('SELECT teamId FROM TeamLeague WHERE leagueId = ?', leagueId).rows;
            tx.executeSql('DELETE FROM TeamLeague WHERE leagueId = ?', leagueId);

            for (var i = 0; i < teams.length; i++) {
                if (tx.executeSql('SELECT * FROM TeamLeague WHERE teamId = ?', teams.item(i).teamId).rows.length == 0) {
                    // remove team, because only containing league is being deleted
                    var icon = tx.executeSql('SELECT icon FROM Team WHERE id = ?', teams.item(i).teamId);
                    fileManager.deleteFile(icon);
                    tx.executeSql('DELETE FROM Team WHERE id = ?', teams.item(i).teamId);
                    tx.executeSql('DELETE FROM Favorite WHERE teamId = ?', teams.item(i).teamId);
                }
            }

            // remove matches
            tx.executeSql('DELETE FROM Fixture WHERE leagueId = ?', leagueId);
            // remove standings
            tx.executeSql('DELETE FROM Standing WHERE leagueId = ?', leagueId);
            // remove meta data
            tx.executeSql('DELETE FROM LocalMatchday WHERE leagueId = ?', leagueId);
        });

    }

    signal leagueTableUpdated()

    /*
     *  Be aware that the league table needs the corresponding teams to work properly. So fetch them first.
     */
    function updateLeagueTable(leagueId) {
        console.log("[LOG] Update league table : ", leagueId)

        var db = init();
        db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT lastupdate, lastUpdatedStandings FROM League WHERE id = ?", leagueId);

            if (rs.rows.length > 0 && rs.rows.item(0).lastUpdatedStandings !== rs.rows.item(0).lastupdate) {
                fetchLeagueTable(leagueId);
            } else {
                // maybe use another signal...
                leagueTableUpdated();
            }
        });
    }

    function fetchLeagueTable(leagueId) {
        requestHelper("competitions/" + leagueId + "/standings", function (standings) {
            var db = init();
            db.transaction(function(tx) {
                var rs = tx.executeSql("SELECT lastupdate, lastUpdatedStandings FROM League WHERE id = ?", leagueId);

                // clear old standing entries
                tx.executeSql('DELETE FROM Standing WHERE leagueId = ?', leagueId);

                // add standings if exists
                if (standings.standings != "") {
                    for (var j = 0; j < standings.standings.length; j++) {
                        for (var i = 0; i < standings.standings[j].table.length; i++) {
                            tx.executeSql("INSERT INTO Standing(leagueId, stage, type, pool, rank, teamId, playedGames, points, goals, goalsAgainst) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            [leagueId, 
                            standings.standings[j].stage, 
                            standings.standings[j].type, 
                            standings.standings[j].group ? standings.standings[j].group : 'noGroup', 
                            standings.standings[j].table[i].position ? standings.standings[j].table[i].position : 0 , 
                            standings.standings[j].table[i].team.id, 
                            standings.standings[j].table[i].playedGames, 
                            standings.standings[j].table[i].points, 
                            standings.standings[j].table[i].goalsFor, 
                            standings.standings[j].table[i].goalsAgainst]);
                        }
                    }
                }
                // set last update of standings to last leagueUpdate => no need to sync anymore
                tx.executeSql('UPDATE League SET lastUpdatedStandings = ? WHERE id = ?', [rs.rows.item(0).lastupdate, leagueId]);
            });
            leagueTableUpdated();
        });
    }

    signal teamsUpdated()

    function updateTeamsByLeague(leagueId) {
        console.log("[LOG] Update league", leagueId);
        var db = init();

        db.transaction(function(tx) {
            var teamCount = tx.executeSql('SELECT * FROM TeamLeague WHERE leagueId = ?', leagueId);
            var league = tx.executeSql('SELECT teamCount FROM League WHERE id = ?', leagueId);

            // only fetch teams, when not all teams are fetched yet or currently being fetched or last fetch older than a week
            if ((!league.rows.item(0) || teamCount.rows.length < league.rows.item(0).teamCount
                    || tx.executeSql("SELECT id FROM League WHERE id = ? AND lastFetchedTeams <= date('now','-7 days')", leagueId).rows.length > 0) &&
                    tx.executeSql('SELECT * FROM FetchingLeagueTeams WHERE leagueId = ?', leagueId).rows.length === 0) {
                fetchTeamsByLeague(leagueId);
            } else {
                teamsUpdated();
            }

        });
    }

    function fetchTeamsByLeague(leagueId) {
        var db = init();
        db.transaction(function(tx) {
            tx.executeSql('INSERT INTO FetchingLeagueTeams(leagueId) VALUES(?)', leagueId);
        });

        requestHelper("competitions/" + leagueId + "/teams", function (teams) {
            var db = init();
            for (var i = 0; i < teams.count; i++) {
                db.transaction(function(tx) {

                    if (teams.teams[i].id) {
                        var rs = tx.executeSql('SELECT id, iconState FROM Team WHERE id = ?', teams.teams[i].id);
                        if (rs.rows.length === 0) {
                            tx.executeSql("INSERT INTO Team(id, name, icon, iconState, code, shortName, lastFetchedFixtures) VALUES(?, ?, ?, ?, ?, ?, date('now','-2 days'))", [teams.teams[i].id, teams.teams[i].name, teams.teams[i].crestUrl, teams.teams[i].crestUrl !== "" ? ModelUtils.IconState.REMOTE : ModelUtils.IconState.NONE, teams.teams[i].tla, teams.teams[i].shortName ? teams.teams[i].shortName : null]);
                            imageDownloader.addDownload(teams.teams[i].id, teams.teams[i].crestUrl);
                        } else {
                            tx.executeSql('DELETE FROM TeamLeague WHERE teamId = ? AND leagueId = ?', [teams.teams[i].id, leagueId]);
                            tx.executeSql("UPDATE Team SET name = ?, code = ?, shortName = ? WHERE id = ?", [teams.teams[i].name, teams.teams[i].tla, teams.teams[i].shortName ? teams.teams[i].shortName : null, teams.teams[i].id]);
                            // download icon if there is none in the db
	                        if (rs.rows.item(0).iconState !== ModelUtils.IconState.LOCAL) {
                                imageDownloader.addDownload(teams.teams[i].id, teams.teams[i].crestUrl);
	                        }
                        }

                        // make sure, the team-league entry exists
                        tx.executeSql('INSERT INTO TeamLeague(teamId, leagueId) VALUES(?, ?)', [teams.teams[i].id, leagueId]);
                        tx.executeSql("UPDATE League SET lastFetchedTeams = date('now') WHERE id = ?", leagueId);
                    }
                });
            }

            db.transaction(function(tx) {
                tx.executeSql('DELETE FROM FetchingLeagueTeams WHERE leagueId = ?', leagueId);
            });
            teamsUpdated();

        });
    }

    signal squadUpdated()

    function fetchSquad(teamId) {
        var db = init();
        db.transaction(function(tx) {
            var rs = tx.executeSql('SELECT * FROM Player WHERE teamId = ?', teamId);
            var us = tx.executeSql('SELECT lastFetchedSquad FROM Team WHERE id = ?', teamId);
            if (rs.rows.length === 0 || us.rows.item(0).lastFetchedSquad < ModelUtils.timeTravel('-7')) {
                requestHelper("teams/" + teamId, function (team) {
                    var db = init();
                    db.transaction(function(tx) {
                        tx.executeSql('DELETE FROM Player WHERE teamId = ?', teamId);
                        for (var i = 0; i < team.squad.length; i++) {
                            tx.executeSql("INSERT INTO Player(id, name, position, dateOfBirth, countryOfBirth, nationality, role, teamId) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", [team.squad[i].id, team.squad[i].name, team.squad[i].position, team.squad[i].dateOfBirth, team.squad[i].countryOfBirth, team.squad[i].nationality, team.squad[i].role.toLowerCase(), team.id]);
                        }
                        tx.executeSql("UPDATE Team SET lastFetchedSquad = date('now') WHERE id = ?", [team.id]);
                    });
                });
            }
        });
        squadUpdated()
    }

    signal fixturesUpdated()

    /*
     * Be aware that the fixtures need the corresponding teams to work properly. So fetch them first.
     */
    function updateFixtures(leagueId, matchday) {
        console.log("[LOG] Update fixtures league = ", leagueId, 'matchday = ', matchday);
        var db = init();

        db.transaction(function(tx) {
            if(tx.executeSql("SELECT currentMatchday FROM League WHERE id = ? AND lastFetchedFixtures <= datetime('now','-5 minutes')", leagueId).rows.length > 0) {
                if(matchday === tx.executeSql("SELECT currentMatchday FROM League WHERE id = ? AND lastFetchedFixtures <= datetime('now','-5 minutes')", leagueId).rows.item(0).currentMatchday) {
                    fetchFixtures(leagueId, matchday);
                }
            } else if (tx.executeSql('SELECT * FROM LocalMatchday WHERE leagueId = ? AND matchday = ?', [leagueId, matchday]).rows.length === 0) {
                fetchFixtures(leagueId, matchday);
            } else {
                // maybe use another signal...
                fixturesUpdated();
            }
        });

    }

    function fetchFixtures(leagueId, matchday) {
        requestHelper("competitions/" + leagueId + "/matches?matchday=" + matchday, function (matches) {
            var db = init();

            db.transaction(function(tx) {
                for (var i = 0; i < matches.count; i++) {
                    tx.executeSql('DELETE FROM Fixture WHERE id = ?', [matches.matches[i].id]);
                    tx.executeSql("INSERT INTO Fixture(id, leagueId, date, matchday, homeTeamId, awayTeamId, homeGoals, awayGoals) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", [matches.matches[i].id, leagueId, matches.matches[i].utcDate, matchday, matches.matches[i].homeTeam.id, matches.matches[i].awayTeam.id, matches.matches[i].score.fullTime.homeTeam, matches.matches[i].score.fullTime.awayTeam]);
                    if (tx.executeSql('SELECT * FROM LocalMatchday WHERE leagueId = ? AND matchday = ?', [leagueId, matchday]).rows.length === 0) {
                        tx.executeSql('INSERT INTO LocalMatchday(leagueId, matchday) VALUES(?, ?)', [leagueId, matchday]);
                    }
                }
                // update lastFetchedFixtures only for the current Matchday
                if(tx.executeSql("SELECT currentMatchday FROM League WHERE id = ?", leagueId).rows.item(0).currentMatchday === matchday) {
                    tx.executeSql("UPDATE League SET lastFetchedFixtures = datetime('now') WHERE id = ?", leagueId);
                }
            });

            fixturesUpdated();
        });
    }

    signal teamFixturesUpdated();

    function updateTeamFixtures(teamId) {
        var db = init();
        var pastweek = ModelUtils.timeTravel('-1')
        var today = ModelUtils.timeTravel('0')

        db.transaction(function(tx) {
            if (tx.executeSql("SELECT id FROM Team WHERE id = ? AND lastFetchedFixtures <= date('now','-1 days')", [teamId]).rows.length > 0) {
                fetchTeamFixtures(teamId);
            } else {
                fetchTeamFixturesByTimeFrame(teamId, pastweek, today);
            }
        });
    }

    function fetchTeamFixtures(teamId) {
        requestHelper("teams/" + teamId + "/matches", function (matches) {
            var db = init();
            db.transaction(function(tx) {
                tx.executeSql('DELETE FROM Fixture WHERE homeTeamId = ? OR awayTeamId = ?', [teamId, teamId]);

                for (var i = 0; i < matches.count; i++) {
                    tx.executeSql('INSERT INTO Fixture(id, leagueId, date, matchday, homeTeamId, awayTeamId, homeGoals, awayGoals) VALUES(?, ?, ?, ?, ?, ?, ?, ?)', [matches.matches[i].id, matches.matches[i].season.id, matches.matches[i].utcDate, matches.matches[i].matchday, matches.matches[i].homeTeam.id, matches.matches[i].awayTeam.id, matches.matches[i].score.fullTime.homeTeam, matches.matches[i].score.fullTime.awayTeam]);

                    // make sure, that teams exist in db
                    if (tx.executeSql('SELECT id FROM Team WHERE id = ? OR id = ?', [matches.matches[i].homeTeam.id, matches.matches[i].awayTeam.id]).rows.length < 2) {
                        updateTeamsByLeague(matches.matches[i].competition.id);
                    }
                }
                tx.executeSql("UPDATE Team SET lastFetchedFixtures = date('now') WHERE id = ?", teamId);
            });

            teamFixturesUpdated();
        });
    }

    function fetchTeamFixturesByTimeFrame(teamId, dateFrom, dateTo) {
        requestHelper("teams/" + teamId + "/matches?dateFrom=" + dateFrom + "\&dateTo=" +dateTo, function (matches) {
            var db = init();
            db.transaction(function(tx) {
                for (var i = 0; i < matches.count; i++) {
                    tx.executeSql('DELETE FROM Fixture WHERE id = ?', [matches.matches[i].id]);
                    tx.executeSql('INSERT INTO Fixture(id, leagueId, date, matchday, homeTeamId, awayTeamId, homeGoals, awayGoals) VALUES(?, ?, ?, ?, ?, ?, ?, ?)', [matches.matches[i].id, matches.matches[i].season.id, matches.matches[i].utcDate, matches.matches[i].matchday, matches.matches[i].homeTeam.id, matches.matches[i].awayTeam.id, matches.matches[i].score.fullTime.homeTeam, matches.matches[i].score.fullTime.awayTeam]);
                }
            });
            teamFixturesUpdated();
        });
    }

    signal timeFrameFixturesUpdated();

    function updateFixturesByTimeFrame(dateFrom, dateTo) {
        requestHelper("matches?dateFrom=" + dateFrom + "\&dateTo=" +dateTo, function (matches) {
            var db = init();
            db.transaction(function(tx) {
                for (var i = 0; i < matches.count; i++) {
                    tx.executeSql('DELETE FROM Fixture WHERE id = ?', matches.matches[i].id);
                    tx.executeSql('INSERT INTO Fixture(id, leagueId, date, matchday, homeTeamId, awayTeamId, homeGoals, awayGoals) VALUES(?, ?, ?, ?, ?, ?, ?, ?)', [matches.matches[i].id, matches.matches[i].season.id, matches.matches[i].utcDate, matches.matches[i].matchday, matches.matches[i].homeTeam.id, matches.matches[i].awayTeam.id, matches.matches[i].score.fullTime.homeTeam, matches.matches[i].score.fullTime.awayTeam]);

                    // make sure, that teams exist in db
                    if (tx.executeSql('SELECT id FROM Team WHERE id = ? OR id = ?', [matches.matches[i].homeTeam.id, matches.matches[i].awayTeam.id]).rows.length < 2) {
                        updateTeamsByLeague(matches.matches[i].competition.id);
                    }
                }
            });
            timeFrameFixturesUpdated();
        });
    }

    signal favoritesChanged()

    // NOTICE: signal not called automatically due to performance optimization
    function addFavorite(teamId) {
        var db = init();
        db.transaction(function (tx) {
            if (tx.executeSql('SELECT teamId FROM Favorite WHERE teamId = ?', teamId).rows.length == 0) {
                var position = 0;
                if (tx.executeSql('SELECT * FROM Favorite ORDER BY position DESC LIMIT 1').rows.item(0)) {
                    position = tx.executeSql('SELECT * FROM Favorite ORDER BY position DESC').rows.item(0).position + 1;
                }
                tx.executeSql('INSERT INTO Favorite(teamId, position) VALUES(?, ?)', [teamId, position]);
            }
        });
    }

    function removeFavorite(teamId) {
        var db = init();
        db.transaction(function (tx) {
            var position = tx.executeSql('SELECT position FROM Favorite WHERE teamId = ?', [teamId]).rows.item(0).position;
            tx.executeSql('DELETE FROM Favorite WHERE teamId = ?', [teamId]);

            tx.executeSql('UPDATE Favorite SET position = (position - 1) WHERE position > ?', [position]);
        });
    }

    function moveFavoriteTeam(teamId, from, to) {
        var db = init();
        db.transaction(function (tx) {
            if (from > to)
                tx.executeSql("UPDATE Favorite SET position = (position + 1) WHERE position >= ? AND position < ?", [to, from]);
            else
                tx.executeSql("UPDATE Favorite SET position = (position - 1) WHERE position > ? AND position <= ?", [from, to]);
            tx.executeSql("UPDATE Favorite SET position = ? WHERE teamId = ?", [to, teamId]);
        });
    }

    function setIcon(icon, teamId) {
        init().transaction(function (tx) {
            tx.executeSql("UPDATE Team SET icon = ?, iconState = ? WHERE id = ?", [icon, ModelUtils.IconState.LOCAL, teamId]);
        });
    }

    function dbRead(query) {
        var rs;
        init().transaction(function (tx) {
            rs = tx.executeSql(query);
        });
        return rs;
    }

    function getAllLeagues() {
        return dbRead("SELECT * FROM League").rows;
    }

    function getLeague(leagueId) {
        return dbRead("SELECT * FROM League WHERE id = " + leagueId).rows.item(0);
    }

    function getLeagueTable(leagueId) {
        return dbRead("SELECT * FROM Standing WHERE type = 'TOTAL' AND leagueId = " + leagueId).rows;
    }

    function getStageLeagueTable(leagueId,stage) {
        return dbRead("SELECT * FROM Standing WHERE type = 'TOTAL' AND leagueId = " + leagueId + " AND stage = " + stage).rows;
    }

    function getStageGroupLeagueTable(leagueId,stage,group) {
        return dbRead("SELECT * FROM Standing WHERE type = 'TOTAL' AND leagueId = " + leagueId + " AND stage = " + ModelUtils.stringIt(stage) + " AND pool = " + ModelUtils.stringIt(group)).rows;
    }
    
    function getGroupLeagueTeam(leagueId,teamId) {
        return dbRead("SELECT pool FROM Standing WHERE type = 'TOTAL' AND leagueId = " + leagueId + " AND teamId = " + teamId).rows.item(0).pool;
    }

    function getStageLeagueTeam(leagueId,teamId) {
        return dbRead("SELECT stage FROM Standing WHERE type = 'TOTAL' AND leagueId = " + leagueId + " AND teamId = " + teamId).rows.item(0).stage;
    }

    function getTableEntry(leagueId, teamId) {
        return dbRead("SELECT * FROM Standing WHERE type = 'TOTAL' AND leagueId = " + leagueId + " AND teamId = " + teamId).rows.item(0);
    }

    function getTeam(teamId) {
        return dbRead("SELECT * FROM Team WHERE id = " + teamId).rows.item(0);
    }
    
    function getSquadTable(teamId) {
        return dbRead("SELECT * FROM Player WHERE teamId = " + teamId +" ORDER BY dateOfBirth ASC").rows;
    }

    function getAllTeams() {
        return dbRead("SELECT * FROM Team ORDER BY name ASC").rows;
    }

    function getTeamLeagues(teamId) {
        return dbRead("SELECT * FROM TeamLeague WHERE teamId = " + teamId).rows;
    }

    function getStagesLeague(leagueId) {
        var rs = dbRead("SELECT DISTINCT stage FROM Standing WHERE leagueId = " + leagueId).rows;
        var stagesLeague = [];
        for (var i = 0; i < rs.length; i++) {
            stagesLeague.push(rs.item(i).stage);
        }
        return stagesLeague;
    }

    function getGroupsLeague(leagueId) {
        var rs = dbRead("SELECT DISTINCT pool FROM Standing WHERE leagueId = " + leagueId).rows;
        var groupsLeague = [];
        for (var i = 0; i < rs.length; i++) {
            groupsLeague.push(rs.item(i).pool);
        }
        return groupsLeague;
    }


    function getFavorites() {
        return dbRead("SELECT * FROM Favorite ORDER BY position ASC").rows;
    }

    function isFavorite(teamId) {
        return dbRead("SELECT * FROM Favorite WHERE teamId = " + teamId).rows.length > 0;
    }

    function getMatchdayFixtures(leagueId, matchday) {
        return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE leagueId = " + leagueId + " AND matchday = " + matchday + " ORDER BY date ASC").rows;
    }

    /* timeFrame needs to be of type ModelUtils.TimeFrame */
    function getFixtures(timeFrame) {
        switch(timeFrame) {
        case ModelUtils.TimeFrame.LAST3DAYS: return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE date < date('now','localtime') AND date > date('now','localtime','-3 day') ORDER BY date DESC").rows;
        case ModelUtils.TimeFrame.TODAY: return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE date < date('now','localtime','+1 days') AND date >= date('now','localtime') ORDER BY date ASC").rows;
        case ModelUtils.TimeFrame.NEXT3DAYS: return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE date < date('now','localtime','+4 days') AND date >= date('now','localtime','+1 day') ORDER BY date ASC").rows;
        default: return -1;
        }
    }

    /* timeFrame needs to be of type ModelUtils.TimeFrame
     * CURRENT: all upcoming fixtures + yesterday's fixture (for convenience)
     * PAST: all fixtures until yesterday (excluding yesterday's game)
     */
    function getTeamFixtures(teamId, timeFrame) {
        switch(timeFrame) {
        case ModelUtils.TimeFrame.PAST: return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE (homeTeamId = " + teamId + " OR awayTeamId = " + teamId + ") AND date <= date('now','-1 day') ORDER BY date DESC").rows;
        case ModelUtils.TimeFrame.CURRENT: return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE (homeTeamId = " + teamId + " OR awayTeamId = " + teamId + ") AND date > date('now','-1 day') ORDER BY date ASC").rows;
        case ModelUtils.TimeFrame.ALL: return dbRead("SELECT *, strftime('%w-%Y-%m-%d/%H:%M', date, 'localtime') as time FROM Fixture WHERE (homeTeamId = " + teamId + " OR awayTeamId = " + teamId + ") ORDER BY date ASC").rows;
        default: return -1;
        }
    }

    function updateApiKey(apikey) {
        console.log("[LOG] update api key ")
        var db = init();
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM Api");
            tx.executeSql("INSERT INTO Api(key) VALUES(?)", [apikey]);
        });
    }
    function getApiKey() {
        var db = init();
        var token;
        var dat;
        db.transaction(function(tx) {
            token = tx.executeSql("SELECT * FROM Api").rows.item(0);
            if (token) {
                token = token.key;
            }
        });
        return token;
    }
    function getStockIcon(name) {
        return Qt.resolvedUrl("/usr/share/icons/ubuntu-mobile/actions/scalable/" + name + ".svg")
    }
}
