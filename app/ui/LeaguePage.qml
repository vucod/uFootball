import QtQuick 2.4
import QtQml.Models 2.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../components"

Page {
    id: page
    property int leagueId
    property int currentMatchday
    property int teamId: -1     // fix for LeagueTableDelegate throwing errors

    Component.onCompleted: {
        var league = datahandler.getLeague(leagueId);
        title = league.name;
        currentMatchday = league.currentMatchday;
        fixtureView.matchday = currentMatchday; // this triggers updateFixtures through the fixtureView
        fixtureView.numberOfMatchdays = league.numberOfMatchdays;

        datahandler.leagueTableUpdated.connect(refreshGroupStage);
        datahandler.leagueTableUpdated.connect(leagueTable.refreshModel);
        datahandler.teamsUpdated.connect(leagueTable.refreshModel);
        datahandler.fixturesUpdated.connect(fixtureView.refreshModel);
        datahandler.teamsUpdated.connect(fixtureView.refreshModel);

        datahandler.updateTeamsByLeague(leagueId);
        datahandler.updateLeagueTable(leagueId);

    }

    Component.onDestruction: {
        datahandler.leagueTableUpdated.disconnect(refreshGroupStage);
        datahandler.leagueTableUpdated.disconnect(leagueTable.refreshModel);
        datahandler.teamsUpdated.disconnect(leagueTable.refreshModel);
        datahandler.fixturesUpdated.disconnect(fixtureView.refreshModel);
        datahandler.teamsUpdated.disconnect(fixtureView.refreshModel);
    }

    header: PageHeader {
        title: page.title

        extension: Row {
            id: tabs
            width: parent.width
            height: children[0].height
            anchors {
                top: parent.top
                left: parent.left
            }
            z: 1

            TabItem {
                text: i18n.tr("Table")
                index: 0
                view: pageListView
                Item {
                    height: parent.height - units.gu(1)
                    width: height

                    visible: pageListView.currentIndex === 0 && leagueTable.group !== 'noGroup'

                    anchors {
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                        rightMargin: units.gu(0.5)
                    }

                    Label {
                        text: leagueTable.group.replace("GROUP_","")
                        color: UbuntuColors.orange
                        fontSize: "medium"

                        anchors {
                            verticalCenter: parent.verticalCenter
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Icon {
                        id: iconBis
                        name: "event"
                        color: UbuntuColors.darkGrey
                        anchors.fill: parent
                    }

                    MouseArea {
                        id: mouseareaBis
                        anchors.fill: parent
                        onClicked: {
                            PopupUtils.open(popoverComponentBis)
                        }
                    }

                    Component {
                        id: popoverComponentBis
                        Dialog {
                            id: popoverBis
                            contentWidth: units.gu(42)

                            title: i18n.tr("Select group and stage")

                            Label {
                                text: i18n.tr('Group:')
                                color: UbuntuColors.darkGrey
                            }

                            Grid {
                                columns: 8
                                spacing: units.gu(0.285)

                                Repeater {
                                    model: datahandler.getGroupsLeague(leagueId)

                                    Label {
                                        text: modelData.replace("GROUP_","")
                                        color: modelData == leagueTable.group ? UbuntuColors.green : UbuntuColors.darkGrey
                                        width: units.gu(4)
                                        height: units.gu(4)
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter

                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                leagueTable.group = modelData
                                                leagueTable.refreshModel()
                                                PopupUtils.close(popoverBis)
                                            }
                                        }
                                    }
                                }
                            }

                            Label {
                                text: i18n.tr("Stage:")
                                color: UbuntuColors.darkGrey
                            }

                            Repeater {
                                model: datahandler.getStagesLeague(leagueId)

                                Label {
                                    text: modelData
                                    color: text == leagueTable.stage ? UbuntuColors.green : UbuntuColors.darkGrey
                                    width: units.gu(4)
                                    height: units.gu(4)
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter

                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            leagueTable.stage = parent.text
                                            leagueTable.refreshModel()
                                            PopupUtils.close(popoverBis)
                                        }
                                    }
                                }
                            }
                            

                            Button {
                                text: i18n.tr("Close")
                                onClicked: PopupUtils.close(popoverBis)
                            }
                        }
                    }
                }
            }

            TabItem {
                id: fixtureTab
                text: i18n.tr("Fixtures")
                index: 1
                view: pageListView

                Item {
                    height: parent.height - units.gu(1)
                    width: height

                    visible: pageListView.currentIndex === 1

                    anchors {
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                        rightMargin: units.gu(0.5)
                    }

                    Label {
                        text: fixtureView.matchday
                        color: UbuntuColors.orange
                        fontSize: "medium"

                        anchors {
                            verticalCenter: parent.verticalCenter
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Icon {
                        id: icon
                        name: "event"
                        color: UbuntuColors.darkGrey
                        anchors.fill: parent
                    }

                    MouseArea {
                        id: mousearea
                        anchors.fill: parent
                        onClicked: {
                            PopupUtils.open(popoverComponent)
                        }
                    }

                    Component {
                        id: popoverComponent
                        Dialog {
                            id: popover
                            contentWidth: units.gu(42)

                            title: i18n.tr("Select matchday")

                            Grid {
                                columns: 8
                                spacing: units.gu(0.285)

                                Repeater {
                                    model: page.currentMatchday - 1

                                    Label {
                                        text: index + 1
                                        color: text == fixtureView.matchday ? UbuntuColors.green : UbuntuColors.darkGrey
                                        width: units.gu(4)
                                        height: units.gu(4)
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter

                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                fixtureView.matchday = parent.text
                                                PopupUtils.close(popover)
                                            }
                                        }
                                    }
                                }

                                UbuntuShape {
                                    width: units.gu(4)
                                    height: units.gu(4)
                                    color: UbuntuColors.orange
                                    aspect: UbuntuShape.Flat

                                    Label {
                                        text: page.currentMatchday
                                        color: "white"
                                        anchors {
                                            verticalCenter: parent.verticalCenter
                                            horizontalCenter: parent.horizontalCenter
                                        }
                                    }

                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            fixtureView.matchday = page.currentMatchday
                                            PopupUtils.close(popover)
                                        }
                                    }
                                }

                                Repeater {
                                    model: fixtureView.numberOfMatchdays - page.currentMatchday

                                    Label {
                                        text: index + page.currentMatchday + 1
                                        color: text == fixtureView.matchday ? UbuntuColors.green : UbuntuColors.darkGrey
                                        width: units.gu(4)
                                        height: units.gu(4)
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter

                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                fixtureView.matchday = parent.text
                                                PopupUtils.close(popover)
                                            }
                                        }
                                    }
                                }
                            }

                            Button {
                                text: i18n.tr("Close")
                                onClicked: PopupUtils.close(popover)
                            }
                        }
                    }
                }
            }
        }
    }

    DividingShadow {
        width: page.width

        anchors {
            top: page.header.bottom
            left: parent.left
        }
    }


    ObjectModel {
        id: pageModel

        LeagueTable {
            id: leagueTable

            height: pageListView.height
            width: page.width
        }

        LeagueFixtureView {
            id: fixtureView

            height: pageListView.height
            width: page.width
        }
    }

    ListView {
        id: pageListView

        width: parent.width

        anchors {
            top: page.header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        model: pageModel
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        interactive: true
        highlightMoveDuration: UbuntuAnimation.BriskDuration
        highlightRangeMode: ListView.StrictlyEnforceRange
    }

    function refreshGroupStage () {
        if (datahandler.getGroupsLeague(leagueId) === null){ 
            leagueTable.group = 'noGroup';
        } else {
            leagueTable.group = datahandler.getGroupsLeague(leagueId)[0];
        }
        if (datahandler.getStagesLeague(leagueId) === null){ 
            leagueTable.stage = 'noStage';
        } else {
            leagueTable.stage = datahandler.getStagesLeague(leagueId)[0];
        }
    }
}
