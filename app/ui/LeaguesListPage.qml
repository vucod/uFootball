import QtQuick 2.4
import Ubuntu.Components 1.3

import "../components"

Page {
    id: root
    title: i18n.tr("Leagues")
    signal clicked(string leagueId)

    header: PageHeader {
        title: root.title

        leadingActionBar {
            numberOfSlots: 0
            actions: tabList.actions
        }
    }

    TabList {
        id: tabList
    }

    Component.onCompleted: {
        datahandler.competitionsUpdated.connect(refreshModel)
        //datahandler.updateFootballSeasons()
        refreshModel();
    }

    ListView {
        id: listView
        anchors {
            top: root.header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        model: ListModel {
            id: listModel
        }

        delegate: ListItem {
            id: delegate
            width: parent.width
            height: nameLabel.height + units.gu(3)

            anchors {
                left: parent.left
                right: parent.right
            }

            contentItem.anchors {
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
                topMargin: units.gu(0.5)
                bottomMargin: units.gu(0.5)
            }

            Label {
                id: nameLabel
                text: name
                fontSize: "large"
                anchors {
                    verticalCenter: parent.verticalCenter
                }
            }

            onClicked: {
                stack.push(Qt.resolvedUrl("LeaguePage.qml"), {"leagueId": model.id});
            }
        }
    }

    function refreshModel() {
        var leagues = datahandler.getAllLeagues();
        listModel.clear();
        for(var i = 0; i < leagues.length; i++) {
            var league = leagues.item(i);
            listModel.append({"id" : league.id, "name" : league.name});
        }
    }
}


