/*
 *  TeamFixtureView displays all available fixtures of one team.
 *  There can only be one instance of this at a time.
 *
 *  MEMO:
 *  - currently only available via TeamOverview => fixtures should be up to date
 *      ~ needs to be updated, when available through other components
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Thumbnailer 0.1

import "../components"
import "../ModelUtils.js" as ModelUtils


Page {
    id: page
    property int teamId
    header: PageHeader {
        title: page.title
    }

    Component.onCompleted: {
        refreshModel()
        datahandler.teamFixturesUpdated.connect(refreshModel);
    }
    Component.onDestruction: {
        datahandler.teamFixturesUpdated.disconnect(refreshModel)
    }

    Flickable {
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        ListView {
            id: fixtureListView

            clip: true
            anchors.fill: parent

            model: ListModel {
                id: fixtureModel
                property bool refreshing: false
            }

            PullToRefresh {
                refreshing: fixtureModel.refreshing
                onRefresh: {
                    console.log("[LOG] Refresh started")
                    fixtureModel.refreshing = true;
                    datahandler.updateTeamFixtures(teamId);
                }
            }

            section.property: "time"
            section.criteria: ViewSection.FullString
            section.delegate: SectionHeaderDelegate { words: ModelUtils.niceDateTime(section) }

            delegate: FixtureDelegate {}
        }

    }
    function refreshModel() {
        console.log("[LOG] Refresh team fixture model")

        fixtureModel.clear();
        var fixtures = datahandler.getTeamFixtures(page.teamId, ModelUtils.TimeFrame.ALL);

        for(var i = 0; i < fixtures.length; i++) {
            var fixture = fixtures.item(i);
            fixtureModel.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));
        }
        fixtureModel.refreshing = false;
    }
}
