import QtQuick 2.4
import Ubuntu.Components 1.3

import "../components"
import "../ModelUtils.js" as ModelUtils


Item {
    property bool updated: false
    property int selectedIndex: 1

    UbuntuShape {
        id: selector
        width: parent.width - units.gu(1)
        height: units.gu(5)
        anchors {
            top: parent.top
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }

        backgroundColor: UbuntuColors.green
        aspect: UbuntuShape.DropShadow
        radius: "medium"

        z: 100

        Row {
            anchors.fill: parent
            property string color: "white"
            property string size: "medium"

            Label {
                id: label0
                text: i18n.tr("Recent")
                color: parent.color
                fontSize: parent.size
                width: parent.width / 3
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                font.weight: selectedIndex == 0 ? Font.Normal : Font.Light

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        selectedIndex = 0;
                        if (!updated) {
                            var pastweek = ModelUtils.timeTravel('-1')
                            var nextweek = ModelUtils.timeTravel('1')
                            datahandler.updateFixturesByTimeFrame(pastweek,nextweek)
                            updated = true;
                        } else {
                            refreshModel();
                        }
                    }
                }
            }

            Label {
                id: label1
                text: i18n.tr("Today")
                color: parent.color
                fontSize: parent.size
                width: parent.width / 3
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                font.weight: selectedIndex == 1 ? Font.Normal : Font.Light

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        selectedIndex = 1;
                        refreshModel();
                    }
                }
            }

            Label {
                id: label2
                text: i18n.tr("Upcoming")
                color: parent.color
                fontSize: parent.size
                width: parent.width / 3
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                font.weight: selectedIndex == 2 ? Font.Normal : Font.Light

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        selectedIndex = 2;
                        refreshModel();
                    }
                }
            }
        }
    }

    Loader {
        id: empty
        anchors {
            top: selector.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    ListView {
        id: fixtureListView

        clip: true
        anchors {
            top: selector.bottom
            topMargin: units.gu(0.5)
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        model: ListModel {
            id: fixtureModel
            property bool refreshing: false
        }

        PullToRefresh {
            refreshing: fixtureModel.refreshing
            onRefresh: {
                fixtureModel.refreshing = true;
                var pastweek = ModelUtils.timeTravel('-1')
                var nextweek = ModelUtils.timeTravel('1')
                datahandler.updateFixturesByTimeFrame(pastweek, nextweek)
            }
        }

        section.property: "time"
        section.criteria: ViewSection.FullString
        section.delegate: SectionHeaderDelegate { words: ModelUtils.niceDateTime(section) }

        delegate: FixtureDelegate {}
    }

    function refreshModel() {
        console.log("[LOG] Refresh fixture model")
        fixtureModel.clear();

        var timeFrame;
        switch(selectedIndex) {
        case 0: timeFrame = ModelUtils.TimeFrame.LAST3DAYS;
                break;
        case 1: timeFrame = ModelUtils.TimeFrame.TODAY;
                break;
        case 2: timeFrame = ModelUtils.TimeFrame.NEXT3DAYS;
                break;
        }
        var fixtures = datahandler.getFixtures(timeFrame);

        for(var i = 0; i < fixtures.length; i++) {
            var fixture = fixtures.item(i);
            fixtureModel.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));
        }

        fixtureModel.refreshing = false;
        if (fixtureModel.count == 0) {
            empty.setSource("NoFixtures.qml")
        }
    }
}
