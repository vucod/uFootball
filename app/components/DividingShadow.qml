import QtQuick 2.4
import Ubuntu.Components 1.3


Rectangle {
    height: units.gu(0.75)
    opacity: 0.3
    z: 2
    gradient: Gradient {
        GradientStop { position: 0.0; color: UbuntuColors.lightGrey}
        GradientStop { position: 1.0; color: "transparent" }
    }
}

