import QtQuick 2.4
import Ubuntu.Components 1.3

// TODO: maybe add support for icon

ListItem {
    property string words
    height: label.height + units.gu(2)
    Label {
        id: label
        text: words
        font.weight: Font.Normal
        anchors {
            left: parent.left
            leftMargin: units.gu(2)
            verticalCenter: parent.verticalCenter
        }
    }
}
