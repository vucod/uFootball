import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import "../ModelUtils.js" as ModelUtils

ListItem {
    id: delegate

    property bool compressed: width < units.gu(48)
    // display date and time insted of goals (only before fixture)
    property bool inlineDate: false

    contentItem.anchors {
        leftMargin: compressed ? units.gu(0.5) : units.gu(1)
        rightMargin: compressed ? units.gu(0.5) : units.gu(1)
        topMargin: units.gu(0.5)
        bottomMargin: units.gu(0.5)
    }

    RowLayout {
        id: layout
        anchors.fill: parent
        spacing: compressed ? units.gu(0.6) : units.gu(0.8)

        property string fontSize: "medium"

        Label {
            id: homeCodeLabel
            text: homeTeamCode
            fontSize: parent.fontSize
            horizontalAlignment: Text.AlignHCenter
            anchors {
                verticalCenter: parent.verticalCenter
            }
            Layout.preferredWidth: (parent.width - centerLoader.width) / 2 - homeIcon.width - (3 * parent.spacing)
        }

        Image {
            id: homeIcon
            fillMode: Image.PreserveAspectFit
            source: ModelUtils.useThumbnailer(homeTeamIcon, homeTeamIconState)
            asynchronous: true
            sourceSize: Qt.size(compressed ? units.gu(4.5) : units.gu(5), units.gu(5))
            Layout.preferredWidth: compressed ? units.gu(4) : units.gu(5)
        }

        Component {
            id: scoreLabelComponent
            Item {
                Label {
                    text: homeTeamGoals
                    fontSize: layout.fontSize
                    anchors {
                        right: scoreSeparator.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Label {
                    id: scoreSeparator
                    text: " : "
                    fontSize: layout.fontSize
                    anchors {
                        verticalCenter: parent.verticalCenter
                        horizontalCenter: parent.horizontalCenter
                    }
                }

                Label {
                    text: awayTeamGoals
                    fontSize: layout.fontSize
                    anchors {
                        left: scoreSeparator.right
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        }

        Component {
            id: dateTime

            Item {
                Label {
                    id: weekday
                    text: { ModelUtils.getWeekday(time) + '  ' +time.split('/')[1] }
                    fontSize: "small"
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        }

        Loader {
            id: centerLoader
            sourceComponent: (inlineDate && homeTeamGoals == "") ? dateTime : scoreLabelComponent

            Layout.preferredWidth: units.gu(6)
        }

        Image {
            id: awayIcon
            fillMode: Image.PreserveAspectFit
            source: ModelUtils.useThumbnailer(awayTeamIcon, awayTeamIconState)
            asynchronous: true
            sourceSize: Qt.size(compressed ? units.gu(4) : units.gu(5), units.gu(5))
            Layout.preferredWidth: compressed ? units.gu(3.5) : units.gu(5)
        }

        Label {
            id: awayCodeLabel
            text: awayTeamCode
            fontSize: parent.fontSize
            horizontalAlignment: Text.AlignHCenter
            anchors {
                verticalCenter: parent.verticalCenter
            }
            Layout.preferredWidth: (parent.width - centerLoader.width) / 2 - homeIcon.width - (3 * parent.spacing)
        }
    }
}
