import QtQuick 2.4
import Ubuntu.Components 1.3

Page {

    header: PageHeader {
        title: i18n.tr("Credits")
    }

    Flickable {
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        contentHeight: contentColumn.height

        Column {
            id: contentColumn
            anchors.fill: parent

            ListItem {
                height: units.gu(4)
                Label {
                    text: i18n.tr("%1 (Creator)").arg("Moritz Weber")
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: Qt.openUrlExternally("https://launchpad.net/ufootball")
                ListItemLayout {
                    title.text: i18n.tr("Initial work")
                    ProgressionSlot {}
                }
            }

            ListItem {
                height: units.gu(4)
                Label {
                    text: i18n.tr("%1 (Maintainer)").arg("Vucodil")
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: Qt.openUrlExternally("mailto:vucodil@mailfence.com")
                ListItemLayout {
                    title.text: i18n.tr("Contact")
                    ProgressionSlot {}
                }
            }

            ListItem {
                height: units.gu(4)
                Label {
                    text: "Daniel Freitag (football-data.org)"
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: Qt.openUrlExternally("http://api.football-data.org/about")
                ListItemLayout {
                    title.text: i18n.tr("About the project")
                    ProgressionSlot {}
                }
            }
            ListItem {
                height: units.gu(4)
                Label {
                    text: i18n.tr("Podbird Project (Code snippets)")
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: Qt.openUrlExternally("https://launchpad.net/podbird")
                ListItemLayout {
                    title.text: i18n.tr("Project page")
                    ProgressionSlot {}
                }
            }

        }
    }
}
