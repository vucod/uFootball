import QtQuick 2.4
import Ubuntu.Components 1.3

import "../components"

Page {
    id: page
    title: i18n.tr("Manage your teams")

    property bool add: false
    // favourites changed: moved, added, removed
    property bool changed: false

    state: add ? "add" : "default"
    states: [
        PageHeadState {
            name: "default"
            head: page.head
            actions: [
               Action {
                   iconName: "add"
                   text: i18n.tr("Add team")
                   onTriggered: {
                       page.state = "add"
                       sortAction.iconName = "sort-listitem"
                       favoritesView.ViewItems.dragMode = false
                       loadAllTeams()
                   }
               },
               Action {
                    id: sortAction
                    iconName: "sort-listitem"
                    text: i18n.tr("Rearrange")
                    onTriggered: {
                        iconName = (iconName == "sort-listitem") ? "close" : "sort-listitem"
                        favoritesView.ViewItems.dragMode = !favoritesView.ViewItems.dragMode
                    }
               }
            ]
        },
        PageHeadState {
            name: "add"
            head: page.head
            actions: [
               Action {
                    iconName: "ok"
                    text: i18n.tr("Done")
                    onTriggered: {
                        page.state = "default"
                        loadFavoriteTeams()
                    }
               }
            ]
            contents: TextField {
                id: searchField
                inputMethodHints: Qt.ImhNoPredictiveText
                placeholderText: i18n.tr("Search teams")
                anchors {
                    rightMargin: units.gu(1)
                }
            }
        }
    ]

    Component.onCompleted: {
        if (state == "default") {
            loadFavoriteTeams()
        } else {
            loadAllTeams()
        }
    }

    Component.onDestruction: {
        if (changed) {
            datahandler.favoritesChanged();
        }
    }

    ListModel {
        id: favoritesModel
    }

    ListModel {
        id: allTeamsModel
    }

    SortFilterModel {
        id: filteredModel
        model: allTeamsModel
        filter.property: "name"
        filter.pattern: page.state == "add" ? RegExp(searchField.text, "gi") : RegExp("", "gi")
    }

    ListView {
        id: favoritesView
        model: page.state == "default" ? favoritesModel : filteredModel

        clip: true
        anchors.fill: parent

        ViewItems.dragMode: false
        ViewItems.onDragUpdated: {
            if (event.status == ListItemDrag.Moving) {
                event.accept = false
            } else if (event.status == ListItemDrag.Dropped) {
                var team = model.get(event.from);
                model.move(event.from, event.to, 1);
                datahandler.moveFavoriteTeam(team.id, event.from, event.to);
                changed = true;
            }
        }

        delegate: TeamDelegate {
            swipeEnabled: page.state == "default"
            leadingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "delete"
                        onTriggered: {
                            datahandler.removeFavorite(model.id);
                            favoritesModel.remove(index);
                            changed = true;
                        }
                    }
                ]
            }
            onClicked: {
                if (page.state == "add") {
                    datahandler.addFavorite(id);
                    changed = true;
                    if (searchField.text == "")
                        allTeamsModel.setProperty(index, "check", true);
                    else
                        loadAllTeams();
                }
            }
        }
    }

    function loadFavoriteTeams() {
        var db = datahandler.init();

        db.transaction(function (tx) {
            favoritesModel.clear();
            var favorites = datahandler.getFavorites();
            for(var i = 0; i < favorites.length; i++) {
                var team = datahandler.getTeam(favorites.item(i).teamId);
                favoritesModel.append({"name": team.name, "icon": team.icon, "id": favorites.item(i).teamId, "check": false, "iconState": team.iconState});
            }
        });
    }

    function loadAllTeams() {
        allTeamsModel.clear();
        var teams = datahandler.getAllTeams();
        for(var i = 0; i < teams.length; i++) {
            var team = teams.item(i);
            allTeamsModel.append({"name": team.name, "icon": team.icon, "id": team.id, "check": datahandler.isFavorite(team.id), "iconState": team.iconState});
        }
    }
}

