import QtQuick 2.4
import Ubuntu.Components 1.3

Page {

    header: PageHeader {
        title: i18n.tr("About")
    }

    Flickable {
        id: flickable

        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        contentHeight: dataColumn.height + units.gu(10) + dataColumn.anchors.topMargin

        Column {
            id: dataColumn

            spacing: units.gu(3)
            anchors {
                top: parent.top; left: parent.left; right: parent.right; topMargin: units.gu(5)
            }

            Image {
                height: width
                width: Math.min(parent.width / 2, parent.height / 2)
                source: "../graphics/uFootball.png"
                anchors.horizontalCenter: parent.horizontalCenter
                sourceSize: Qt.size(width, height)
            }

            Column {
                width: parent.width
                Label {
                    width: parent.width
                    fontSize: "x-large"
                    font.weight: Font.DemiBold
                    horizontalAlignment: Text.AlignHCenter
                    text: "uFootball"
                }
                Label {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("Version %1").arg(main.version)
                }
            }

            Column {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                }
                Label {
                    fontSize: "small"
                    width: parent.width
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("Released under the terms of the GNU GPL v3")
                }
            }

            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                fontSize: "small"
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Source code available on %1").arg("<a href=\"https://gitlab.com/vucodil/ufootball\">gitlab.com</a>")
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}


