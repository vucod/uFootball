# uFootball App

Get the last scores of football games

## Important information

The app fetches football data from [football-data.org](www.football-data.org/) using the v2 API.
You need to create an account on this website to get access to the data.

## Overview


<img src="doc/select_apikey.png"  width="275" alt="Api key image"> 
<img src="doc/Home.png"  width="250" alt="Home image"> 
<img src="doc/Leagues.png"  width="250" alt="Leagues image"> 
<img src="doc/League.png"  width="250" alt="League image"> 
<img src="doc/Players.png"  width="250" alt="Players image"> 


## Initial work

Initial work from Moritz Weber : https://launchpad.net/ufootball.

